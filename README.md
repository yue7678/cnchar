# idea中文字符替换插件

#### 主要功效
* 1.自动替换中文的逗号、分号、句号、括号等代码常用字符; 
* 2.再也不用反复切换输入法了;
* 3.支持JetBrains 全家桶;
* 4.程序员狂躁症的发作次数明显减少;
* 5.键盘寿命明显增加;
* 6.代码bug明显减少;
* 7.编码效率提升百分之好几十;
* 8.产品经理终于可以下床了！


#### 安装教程

1.  下载 cnchar.jar [点此下载](https://gitee.com/roseboy/cnchar/blob/master/cnchar.jar)
2.  IDEA->File->Settings->Plugins->右上角齿轮->Install Plugin from Disk->选择cnchar.jar

#### 使用说明

1.  当输入以下字符时( ，。；！（）「」《》), 会自动转换成对应的英文字符;
2.  若想输入以上中文字符,先输入/ , 再输入想要的中文字符;

![截图](https://oscimg.oschina.net/oscnet/up-9c80e9e14ebc6015f7949e3905a522ee9e0.png)
